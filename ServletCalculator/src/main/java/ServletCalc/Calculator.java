package ServletCalc;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


public class Calculator extends HttpServlet {
	
//	Post Method
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		res.setContentType("text/html");
		PrintWriter out = null;
//		Try block
		try {
			out= res.getWriter();
			out.println("<center>");
			int a =Integer.parseInt(req.getParameter("n1"));
			int b =Integer.parseInt(req.getParameter("n2"));
			int c = 0;
			
			String output = req.getParameter("btn");
			
//			Addition
			if(output.equals("+")) {
				c = a+b;
			}
//			Subtraction
			if(output.equals("-")) {
				c = a-b;
			}
//			Multiplication
			if(output.equals("*")) {
				c = a*b;
			}
//			Division
			if(output.equals("/")) {
				c = a/b;
			}
			
			out.println(" <h3> " + a + " " + output+" " + b + " = " + c + " <h3> ");
			
			}
//		Catch Block
		catch(Exception e){
			out.println("Error : " + e.getMessage());
		}
//		Finally Block
		finally {
			out.println("<br>");
			out.println("To Goto the Calculator <a href=index.html> Click Here </a>");
			out.println("</center>");
		}
		
	}
//	Getmethod
	protected void doget(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		res.setContentType("text/html");
		PrintWriter out = null;
//		Try block
		try {
			out= res.getWriter();
			out.println("<center>");
			int a =Integer.parseInt(req.getParameter("n1"));
			int b =Integer.parseInt(req.getParameter("n2"));
			int c = 0;
			
			String output = req.getParameter("btn");
			
//			Addition
			if(output.equals("+")) {
				c = a+b;
			}
//			Subtraction
			if(output.equals("-")) {
				c = a-b;
			}
//			Multiplication
			if(output.equals("*")) {
				c = a*b;
			}
//			Division
			if(output.equals("/")) {
				c = a/b;
			}
			
			out.println(" <h3> " + a + " " + output+" " + b + " = " + c + " <h3> ");
			
			}
//		Catch Block
		catch(Exception e){
			out.println("Error : " + e.getMessage());
		}
//		Finally Block
		finally {
			out.println("<br>");
			out.println("To Goto the Calculator <a href=index.html> Click Here </a>");
			out.println("</center>");
		}
		
		
	}

}
